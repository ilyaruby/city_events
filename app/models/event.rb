class Event < ApplicationRecord
  has_many :event_topics
  has_many :topics, through: :event_topics
  belongs_to :city
end
