class Topic < ApplicationRecord
  has_many :events_topics
  has_many :events, through: :event_topics
end
