EventTopic.destroy_all
Event.destroy_all
Topic.destroy_all
Filter.destroy_all
Notification.destroy_all
User.destroy_all

avia      = Topic.create name: 'Авиамоделизм'
wrestling = Topic.create name: 'Борьба'
velo      = Topic.create name: 'Велосипед'
urban     = Topic.create name: 'Градостроительство'
children  =  Topic.create name: 'Дети'

moscow = City.create name: 'Москва'
kazan  = City.create name: 'Казань'
sever  = City.create name: 'Мурманск'

user = User.create
        
event1 = Event.create name: 'Проблемы квадрокоптеров в средней полосе', city: kazan, topics: [ avia, children ]
event2 = Event.create name: 'Проблемы велосипедистов в средней полосе', city: kazan, topics: [ avia, children ]
event3 = Event.create name: 'Проблемы велосипедистов на крайнем севере', city: sever, topics: [ avia, children ]

Filter.create name: 'Велосипедисты в моем городе', city: kazan, user: user, topic: velo

Notification.create user: user, event: event2

