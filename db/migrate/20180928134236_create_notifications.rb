class CreateNotifications < ActiveRecord::Migration[5.2]
  def change
    create_table :notifications do |t|
      t.string :name
      t.references :user, foreign_key: true
      t.references :event, foreign_key: true
      t.boolean :unread, default: true

      t.timestamps
    end
  end
end
