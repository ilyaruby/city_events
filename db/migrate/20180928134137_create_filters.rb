class CreateFilters < ActiveRecord::Migration[5.2]
  def change
    create_table :filters do |t|
      t.string :name
      t.references :user, foreign_key: true
      t.references :city, foreign_key: true
      t.references :topic, foreign_key: true

      t.timestamps
    end
  end
end
