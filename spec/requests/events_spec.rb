require 'rails_helper'
require "support/shared/topics.rb"
require "support/shared/cities.rb"

RSpec.describe "Events", type: :request do
  include_context "topics"
  include_context "cities"

  let(:event_aero_kazan) { create :event, topics: [aero], city: kazan, name: 'АэроКазань' }
  let(:event_velo_kazan) { create :event, topics: [velo], city: kazan, name: 'ВелоКазань' }
  let(:event_aero_murmansk) { create :event, topic: [aero], city: murmansk, name: 'АэроМурманск' }
  let(:event_velo_murmansk) { create :event, topics: [velo], city: murmansk, name: 'ВелоМурманск' }

  before do
    #event_aero_kazan
    #event_aero_murmansk
    #event_velo_kazan
    event_velo_murmansk
  end

  describe "GET /events" do
    it "works! (now write some real specs)" do
      get events_path
      expect(response).to have_http_status(200)
    end
  end
end
