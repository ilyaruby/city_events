require 'rails_helper'
require "support/shared/topics.rb"

RSpec.describe "Topics", type: :request do
  include_context "topics"

  describe "GET /topics" do
    before { get topics_path }

    it 'returns a correctly serialized list of topics' do
      expect(JSON.parse(response.body)).to include(
        including(
          'id' => avia.id,
          'name' => avia.name
        ),
        including(
          'id' => velo.id,
          'name' => velo.name
        )
      )
    end
  end
end
