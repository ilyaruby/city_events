require 'rails_helper'
require "support/shared/cities.rb"

RSpec.describe "Cities", type: :request do
  include_context "cities"

  describe "GET /cities" do
    before do
      get cities_path
    end

    it 'returns a correctly serialized list of cities' do
      expect(JSON.parse(response.body)).to include(
        including(
            'id' => kazan.id,
            'name' => kazan.name
        ),
        including(
            'id' => murmansk.id,
            'name' => murmansk.name
        )
      )
    end
  end
end
