RSpec.shared_context 'topics', :shared_context => :metadata do
  let(:avia) { create :topic, name: 'Авиамоделизм' }
  let(:velo) { create :topic, name: 'Велосипед'}

  before do
    avia
    velo
  end
end
