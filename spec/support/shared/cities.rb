RSpec.shared_context 'cities', :shared_context => :metadata do
  let(:kazan) { create :city, name: 'Казань' }
  let(:murmansk) { create :city, name: 'Мурманск' }

  before do
    kazan
    murmansk
  end
end
